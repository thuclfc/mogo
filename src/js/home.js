$(document).ready(function () {
    //menu
    $('.navbar-toggler').on('click',function () {
       $('.navbar-collapse,.modal-menu').addClass('is-open');
    });
    $('.modal-menu,.close-menu').on('click',function () {
        $('.navbar-collapse,.modal-menu').removeClass('is-open');
    });

    //accordion
    var acc_title = $('.accordion h4');
    var acc_content = $('.accordion__content');
    var acc_active = $('.accordion__item.active');
    acc_content.hide();
    acc_active.find(acc_content).slideToggle();
    acc_title.click(function () {
        acc_title.parent().removeClass('active');
        $(this).parent().addClass('active');
        acc_content.slideUp();
        if(acc_content.parent('active')){
            $(this).parent().find(acc_content).slideToggle();
        }
        return false;
    });

    //slider feedback
    $('.feedback__list').slick({
        centerMode: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: false,
        centerPadding: 0,
        fade: true,
        dots: false,
        autoplay: true,
        arrows: true
    });
});
$(window).scroll(function () {
    if($(this).scrollTop() > 100){
        $('header').addClass('is-scroll');
    }else{
        $('header').removeClass('is-scroll');
    }
});